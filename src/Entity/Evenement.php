<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Evenement
 */
#[ORM\Table(name: 'evenement')]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=> ["evenement:read"]],
    denormalizationContext: ["groups"=> ["evenement:write"]]
)]

class Evenement
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 60, nullable: false, options: ['fixed' => true])]
    #[Groups(["participe:read", "evenement:read", "evenement:write"])]
    private $nom;

    /**
     * @var string
     */
    #[ORM\Column(name: 'lieu', type: 'string', length: 60, nullable: false, options: ['fixed' => true])]
    #[Groups(["participe:read", "evenement:read", "evenement:write"])]
    private $lieu;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'nombre_spectateur', type: 'integer', nullable: true)]
    #[Groups(["evenement:read", "evenement:write"])]
    private $nombreSpectateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getNombreSpectateur(): ?int
    {
        return $this->nombreSpectateur;
    }

    public function setNombreSpectateur(?int $nombreSpectateur): self
    {
        $this->nombreSpectateur = $nombreSpectateur;

        return $this;
    }


}
