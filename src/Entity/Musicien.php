<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Musicien
 */
#[ORM\Table(name: 'musicien')]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=> ["musicien:read"]],
    denormalizationContext: ["groups"=> ["musicien:write"]]
)]
#[ApiFilter(SearchFilter::class, properties: ["nom"=>"exact", "instrument"=>"partial"])]
#[ApiFilter(OrderFilter::class, properties: ["nom"],arguments: ["orderParameterName"=>"order"])]

class Musicien
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'prenom', type: 'string', length: 50, nullable: false, options: ['fixed' => true])]
    #[Groups(["groupe:read", "musicien:write"])]
    private $prenom;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 50, nullable: false, options: ['fixed' => true])]
    #[Groups(["groupe:read", "musicien:write"])]
    private $nom;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date_naissance', type: 'date', nullable: false)]
    #[Groups(["musicien:read", "musicien:write"])]
    private $dateNaissance;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'date_deces', type: 'date', nullable: true)]
    #[Groups(["musicien:read", "musicien:write"])]
    private $dateDeces;

    /**
     * @var string
     */
    #[ORM\Column(name: 'instrument', type: 'text', length: 65535, nullable: false)]
    #[Groups(["groupe:read", "musicien:read", "musicien:write"])]
    private $instrument;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getDateDeces(): ?\DateTimeInterface
    {
        return $this->dateDeces;
    }

    public function setDateDeces(?\DateTimeInterface $dateDeces): self
    {
        $this->dateDeces = $dateDeces;

        return $this;
    }

    public function getInstrument(): ?string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): self
    {
        $this->instrument = $instrument;

        return $this;
    }


}
