<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Contrat
 */
#[ORM\Table(name: 'contrat')]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=> ["contrat:read"]],
    denormalizationContext: ["groups"=> ["contrat:write"]]
)]

class Contrat
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date_contrat', type: 'date', nullable: false)]
    #[Groups(["contrat:read", "contrat:write"])]
    private $dateContrat;

    /**
     * @var int|null
     */
    #[ORM\Column(name: 'cachet', type: 'integer', nullable: true, options: ['default' => 1000])]
    #[Groups(["participe:read", "contrat:read", "contrat:write"])]
    private $cachet = 1000;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateContrat(): ?\DateTimeInterface
    {
        return $this->dateContrat;
    }

    public function setDateContrat(\DateTimeInterface $dateContrat): self
    {
        $this->dateContrat = $dateContrat;

        return $this;
    }

    public function getCachet(): ?int
    {
        return $this->cachet;
    }

    public function setCachet(?int $cachet): self
    {
        $this->cachet = $cachet;

        return $this;
    }


}
