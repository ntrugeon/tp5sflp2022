<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\Collection;

/**
 * Groupe
 */
#[ORM\Table(name: 'groupe')]
#[ORM\Index(name: 'label', columns: ['label'])]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=> ["groupe:read"]],
    denormalizationContext: ["groups"=> ["groupe:write"]]
)]

class Groupe
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(["groupe:read"])]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 60, nullable: false, options: ['fixed' => true])]
    #[Groups(["groupe:read", "groupe:write", "participe:read"])]
    private $nom;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date_creation', type: 'date', nullable: false)]
    #[Groups(["groupe:read", "groupe:write"])]
    private $dateCreation;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'date_fin', type: 'date', nullable: true)]
    #[Groups(["groupe:read", "groupe:write"])]
    private $dateFin;

    /**
     * @var Label
     */
    #[ORM\JoinColumn(name: 'label', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Label')]
    #[Groups(["groupe:read", "groupe:write"])]
    private $label;

    #[ORM\OneToMany(targetEntity: 'App\Entity\Membre', mappedBy: "groupe")]
    #[Groups(["groupe:read"])]
    private $membres;

    public function __construct()
    {
        $this->membres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getLabel(): ?Label
    {
        return $this->label;
    }

    public function setLabel(?Label $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Membre[]
     */
    public function getMembres(): Collection
    {
        return $this->membres;
    }

    public function addMembre(Membre $membre): self
    {
        if (!$this->membres->contains($membre)) {
            $this->membres[] = $membre;
            $membre->setGroupe($this);
        }

        return $this;
    }

    public function removeMembre(Membre $membre): self
    {
        if ($this->membres->removeElement($membre)) {
            // set the owning side to null (unless already changed)
            if ($membre->getGroupe() === $this) {
                $membre->setGroupe(null);
            }
        }

        return $this;
    }


}
