<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Membre
 */
#[ORM\Table(name: 'membre')]
#[ORM\Index(name: 'musicien', columns: ['musicien'])]
#[ORM\Index(name: 'groupe', columns: ['groupe'])]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=> ["membre:read"]],
    denormalizationContext: ["groups"=> ["membre:write"]]
)]
class Membre
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date_debut_participation', type: 'date', nullable: false)]
    #[Groups(["groupe:read", "membre:read", "membre:write"])]
    private $dateDebutParticipation;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'date_fin_participation', type: 'date', nullable: true)]
    #[Groups(["groupe:read", "membre:read", "membre:write"])]
    private $dateFinParticipation;

    /**
     * @var Musicien
     */
    #[ORM\JoinColumn(name: 'musicien', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Musicien')]
    #[Groups(["groupe:read", "membre:read", "membre:write"])]
    private $musicien;

    /**
     * @var Groupe
     */
    #[ORM\JoinColumn(name: 'groupe', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Groupe', inversedBy: 'membres')]
    #[Groups(["membre:read", "membre:write"])]
    private $groupe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebutParticipation(): ?\DateTimeInterface
    {
        return $this->dateDebutParticipation;
    }

    public function setDateDebutParticipation(\DateTimeInterface $dateDebutParticipation): self
    {
        $this->dateDebutParticipation = $dateDebutParticipation;

        return $this;
    }

    public function getDateFinParticipation(): ?\DateTimeInterface
    {
        return $this->dateFinParticipation;
    }

    public function setDateFinParticipation(?\DateTimeInterface $dateFinParticipation): self
    {
        $this->dateFinParticipation = $dateFinParticipation;

        return $this;
    }

    public function getMusicien(): ?Musicien
    {
        return $this->musicien;
    }

    public function setMusicien(?Musicien $musicien): self
    {
        $this->musicien = $musicien;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }


}
