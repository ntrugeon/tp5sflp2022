<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Label
 */
#[ORM\Table(name: 'label')]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=> ["label:read"]],
    denormalizationContext: ["groups"=> ["label:write"]]
)]

class Label
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    private $id;

    /**
     * @var string
     */
    #[ORM\Column(name: 'nom', type: 'string', length: 50, nullable: false, options: ['fixed' => true])]
    #[Groups(["groupe:read", "label:read", "label:write"])]
    private $nom;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'type_production', type: 'text', length: 65535, nullable: true)]
    #[Groups(["label:read", "label:write"])]
    private $typeProduction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTypeProduction(): ?string
    {
        return $this->typeProduction;
    }

    public function setTypeProduction(?string $typeProduction): self
    {
        $this->typeProduction = $typeProduction;

        return $this;
    }


}
