<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Participe
 */
#[ORM\Table(name: 'participe')]
#[ORM\Index(name: 'contrat', columns: ['contrat'])]
#[ORM\Index(name: 'evenement', columns: ['evenement'])]
#[ORM\Index(name: 'groupe', columns: ['groupe'])]
#[ORM\Entity]
#[ApiResource(
    normalizationContext: ["groups"=>["participe:read"]],
    denormalizationContext: ["groups"=> ["participe:write"]]
)]
#[ApiFilter(SearchFilter::class, properties: ["evenement.nom"=> "partial"])]
#[ApiFilter(OrderFilter::class, properties: ["dateDebut"], arguments: ['orderParameterName'=>"order"])]

class Participe
{
    /**
     * @var int
     */
    #[ORM\Column(name: 'id', type: 'integer', nullable: false)]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[Groups(["participe:read"])]
    private $id;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'date_debut', type: 'date', nullable: false)]
    #[Groups(["participe:read"])]
    private $dateDebut;

    /**
     * @var \DateTime|null
     */
    #[ORM\Column(name: 'date_fin', type: 'date', nullable: true)]
    #[Groups(["participe:read"])]
    private $dateFin;

    /**
     * @var Contrat
     */
    #[ORM\JoinColumn(name: 'contrat', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Contrat')]
    #[Groups(["participe:read"])]
    private $contrat;

    /**
     * @var Evenement
     */
    #[ORM\JoinColumn(name: 'evenement', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Evenement')]
    #[Groups(["participe:read"])]
    private $evenement;

    /**
     * @var Groupe
     */
    #[ORM\JoinColumn(name: 'groupe', referencedColumnName: 'id')]
    #[ORM\ManyToOne(targetEntity: 'Groupe')]
    #[Groups(["participe:read"])]
    private $groupe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getContrat(): ?Contrat
    {
        return $this->contrat;
    }

    public function setContrat(?Contrat $contrat): self
    {
        $this->contrat = $contrat;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }


}
